package controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class QUnitController extends Controller {
    public static Result qunit() {
        return ok(views.html.qunit.render());
    }
}
