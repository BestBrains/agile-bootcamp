var TodoViewModel = function TodoViewModel(server) {
    "use strict";
    var self = this;
    self.server = server;

    self.newTodo = ko.observable("");
    self.todos = ko.mapping.fromJS([]);

    self.getTodos = function getTodos() {
        self.server.getTodos(function (data) {
            ko.mapping.fromJS(data, new TodoModelMapping(self.server), self.todos);
        });
    };
    self.getTodos();

    self.createTodo = function createTodos() {
        var todo = {
            label: self.newTodo(),
            completed: false
        };

        self.server.createTodo(todo, self.getTodos);

        self.todos.push(ko.mapping.fromJS(todo));
        self.newTodo("");
    };

    self.removeTodo = function removeTodos(target) {
        self.server.removeTodo(target, self.getTodos);
        self.todos.remove(target);
    };
};
